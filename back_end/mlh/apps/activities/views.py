import json

from django.shortcuts import render
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Activity, ActivitySignUP
from .serializers import ActivityListSerializer, ActivityDetailSerializer, ActivitySignupSerializer
# Create your views here.
from rest_framework.generics import GenericAPIView, CreateAPIView


class ActivityView(APIView):
    def get(self, request, id):
        activity = Activity.objects.get(id=id)
        serializers = ActivityDetailSerializer(activity)
        return Response(serializers.data)


class ActivityViewSet(GenericAPIView):
    queryset = Activity.objects.all().order_by('-start_time')
    serializer_class = ActivityListSerializer

    def get(self, request):
        book = self.get_queryset()
        serializer = self.get_serializer(book, many=True)
        return Response(serializer.data)


class ActivitySignupView(APIView):
    def get(self, request, id, user_id):
        try:
            activity = ActivitySignUP.objects.filter(avtivity_id=id, user_id=user_id).first()
            serializers = ActivitySignupSerializer(activity)
            return Response(serializers.data)
        except Exception as e:
            response = Response(status=400)
        return response
    # def get(self, request, id, user_id):
    #     try:
    #         activity = ActivitySignUP.objects.get(avtivity_id=id, user_id=user_id)
    #         serializers = ActivitySignupSerializer(activity)
    #         return Response(serializers.data)
    #     else:
    #         return Response(json.dumps(''))


class ActivitySignupCreateView(CreateAPIView):
    # permission_classes = [IsAuthenticated]
    serializer_class = ActivitySignupSerializer


