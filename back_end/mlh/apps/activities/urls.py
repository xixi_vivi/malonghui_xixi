from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^activity/$', views.ActivityViewSet.as_view()),
    url(r'^activity/(?P<id>\d+)/(?P<user_id>\d+)/$', views.ActivitySignupView.as_view()),
    url(r'^activity/(?P<id>\d+)/$', views.ActivityView.as_view()),
    url(r'^activity/(?P<id>\d+)/(?P<user_id>\d+)/(?P<signup_status>.+)/$', views.ActivitySignupCreateView.as_view()),

]

