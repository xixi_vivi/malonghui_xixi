from rest_framework import serializers

from .models import Activity, ActivitySignUP


class ActivityListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = ('id', 'title', 'image', 'city', 'start_time', 'status')


class ActivityDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Activity
        fields = '__all__'


class ActivitySignupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ActivitySignUP
        fields = '__all__'
