from django.db import models


# Create your models here.
class Activity(models.Model):
    STATUS_CHOICE = ((1, "立即报名"), (2, "报名截止"), (3, "活动结束"))
    # image = models.CharField(verbose_name="图片", max_length=128)

    image = models.ImageField(verbose_name="图片", upload_to="activity")
    title = models.CharField(verbose_name="标题", max_length=32)
    city = models.CharField(verbose_name="举办城市", max_length=64)
    address = models.CharField(verbose_name="地址", max_length=128)
    status = models.SmallIntegerField(choices=STATUS_CHOICE, verbose_name="状态")
    start_time = models.DateTimeField(verbose_name="开始时间")
    end_time = models.DateTimeField(verbose_name="结束时间")
    apply_time = models.DateTimeField(verbose_name="报名截止时间")
    sponsor = models.CharField(verbose_name="主办方", max_length=64)
    abstract = models.TextField(verbose_name="简介")
    description = models.TextField(verbose_name="详情")
    link = models.CharField(verbose_name="链接", max_length=128)
    is_delete = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        db_table = "tb_activity"
        verbose_name = "活动"
        verbose_name_plural = verbose_name


class ActivitySignUP(models.Model):
    user_id = models.IntegerField(verbose_name="用户ID")
    avtivity_id = models.IntegerField(verbose_name="活动ID")
    signup_status = models.BooleanField(verbose_name="是否报名",default=False)

    class Meta:
        db_table = "tb_activity_signup"
        verbose_name = "活动报名"
        verbose_name_plural = verbose_name