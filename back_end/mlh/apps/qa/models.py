from django.db import models

# Create your models here.
from mlh.utils.models import BaseModel


class Answers(models.Model):
    # fields = ('id', 'user_name', 'create_time', 'title', 'q_content', 'author_name', 'a_content', )
    id = models.AutoField(verbose_name='回答', primary_key=True)
    useful = models.IntegerField(default=0, verbose_name='有用')
    answer = models.IntegerField(default=0, verbose_name='回答')
    author_name = models.CharField(max_length=32, verbose_name='回答者')
    read = models.IntegerField(default=0, verbose_name='浏览量')
    create_time = models.DateTimeField(max_length=32, verbose_name='提问时间')
    answer_time = models.DateTimeField(max_length=32, verbose_name='回答时间')
    update_time = models.DateTimeField(max_length=32, verbose_name='更新时间')
    title = models.CharField(max_length=32, verbose_name='问题标题')
    user_name = models.CharField(default=None, max_length=32, verbose_name='提问者')
    a_content = models.TextField(default=None, verbose_name='回答详情')
    q_content = models.TextField(default=None, verbose_name='问题详情')
    category = models.IntegerField(verbose_name='回答分类')

    class Meta:
        db_table = 'qa_answers'  # 指明数据库表名
        verbose_name = '回答列表'  # 在admin站点中显示的名称
        verbose_name_plural = verbose_name  # 显示的复数名称

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.title


class Questions(models.Model):
    id = models.AutoField(default=1, verbose_name='问题', primary_key=True)
    # user_name = models.CharField(default=None, max_length=32, verbose_name='提问者')
    # create_time = models.DateTimeField(max_length=32, verbose_name='提问时间')
    # q_content = models.TextField(default=None, verbose_name='问题详情')
    title = models.CharField(default=None, max_length=32, verbose_name='问题标题')
    # a_content = models.TextField(default=None, verbose_name='回答详情')
    # author_name = models.CharField(default=None, max_length=32, verbose_name='回答者')
    qa = models.ForeignKey(Answers, default=None, max_length=32, verbose_name='外键问答')

    class Meta:
        db_table = 'qa_questions'  # 指明数据库表名
        verbose_name = '问题列表'  # 在admin站点中显示的名称
        verbose_name_plural = verbose_name  # 显示的复数名称

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.title


class CategoryId(models.Model):
    id = models.AutoField(verbose_name='分类ID', primary_key=True)
    name = models.CharField(max_length=32, verbose_name='分类名称')

    class Meta:
        db_table = 'qa_category'
        verbose_name = '回答分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.name


class Tags(models.Model):
    id = models.AutoField(verbose_name='标签ID', primary_key=True)
    name = models.CharField(max_length=32, verbose_name='标签名称')
    people = models.IntegerField(verbose_name='关注人数')
    abstract = models.CharField(max_length=1024, verbose_name='标签简介')
    image = models.ImageField(max_length=9999, verbose_name='标签图片', upload_to="qa_tags")
    answers_tag = models.ForeignKey(Answers, max_length=32, verbose_name='问题标签名称')

    class Meta:
        db_table = 'qa_tags'
        verbose_name = '标签分类'
        verbose_name_plural = verbose_name

    def __str__(self):
        """定义每个数据对象的显示信息"""
        return self.name
