from django.conf.urls import url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^answers/$', views.AnswersViewSet.as_view({"get":"list"})),
    url(r'^answers/(?P<user_name>\w+)/$', views.MyQuestionView.as_view()),
    url(r'^answers/(?P<author_name>\w+)/answer/$', views.MyAnswerView.as_view()),
    url(r'^answers_detail/(?P<pk>\d+)$', views.AnswersViewSet.as_view({"get": "retrieve"})),
    url(r'^tags_all/', views.TagsViewSet.as_view({"get": "list"})),
    url(r'^tags_detail/(?P<pk>\d+)$', views.TagsViewSet2.as_view({"get": "retrieve"})),
    url(r'^tags_custom/', views.TagsViewSet.as_view({"get": "list"})),
    url(r'^all-tags/', views.TagsViewSet.as_view({"get": "list"})),

]


