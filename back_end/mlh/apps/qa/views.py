from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework.filters import OrderingFilter

from qa.models import Answers, Questions, Tags, CategoryId
from qa.serializers import AnswersListSerializers, AnswersDetailSerializers, TagsListSerializers, TagsDetailSerializers, \
    CategoryIdSerializer, QuestionsSerializer


class AnswersViewSet(ReadOnlyModelViewSet):
    """
    返回回答列表
    """
    queryset = Answers.objects.all().order_by('create_time')

    def get_serializer_class(self):
        if self.action == 'list':
            return AnswersListSerializers
        else:
            return AnswersDetailSerializers


class TagsViewSet(ReadOnlyModelViewSet):
    """
    返回标签列表
    """
    queryset = Tags.objects.all().order_by('people')

    def get_serializer_class(self):
        if self.action == 'list':
            return TagsListSerializers
        # elif self.action == 'list':
        #     return TagsCustomSerializers
        # else:


class TagsViewSet2(ReadOnlyModelViewSet):
    """
    返回标签详情列表
    """

    queryset = Tags.objects.all().order_by('people')

    def get_serializer_class(self):
        if self.action == 'retrieve':
            return TagsDetailSerializers


class MyQuestionView(APIView):
    """回答"""
    def get(self, request, user_name):
        # user = User.objects.get(id=user_id)
        # author_name = user.username
        # print(author_name)
        answer = Answers.objects.filter(user_name=user_name)
        serializer = AnswersListSerializers(answer, many=True)
        return Response(serializer.data)


class MyAnswerView(APIView):
    """提问"""
    def get(self, request, author_name):
        # user = User.objects.get(id=user_id)
        # author_name = user.username
        # print(author_name)
        answer = Answers.objects.filter(author_name=author_name)
        serializer = AnswersListSerializers(answer, many=True)
        return Response(serializer.data)


class CategoryViewSet(ReadOnlyModelViewSet):

    queryset = Answers.objects.all().order_by('create_time')

    def get_serializer_class(self):
        if self.action == 'list':
            return CategoryIdSerializer


class QuestionsViewSet(ReadOnlyModelViewSet):

    queryset = Answers.objects.all().order_by('create_time')

    def get_serializer_class(self):
        if self.action == 'list':
            return AnswersListSerializers
