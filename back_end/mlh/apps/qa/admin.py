from django.contrib import admin
from .models import Answers, Tags, CategoryId, Questions

# Register your models here.

admin.site.register(Answers)
admin.site.register(Questions)
admin.site.register(Tags)
admin.site.register(CategoryId)