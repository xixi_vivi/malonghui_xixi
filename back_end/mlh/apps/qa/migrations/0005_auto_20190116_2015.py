# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-16 20:15
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qa', '0004_questions_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tags',
            name='image',
            field=models.ImageField(max_length=9999, upload_to='qa_tags', verbose_name='标签图片'),
        ),
    ]
