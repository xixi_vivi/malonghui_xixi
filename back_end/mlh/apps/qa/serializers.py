from django.shortcuts import render
from rest_framework import serializers
from qa.models import Answers, Questions, Tags, CategoryId


class AnswersListSerializers(serializers.ModelSerializer):
    class Meta:
        model = Answers
        fields = ('id', 'author_name', 'useful', 'title', 'answer', 'read', 'create_time', 'user_name',)


class AnswersDetailSerializers(serializers.ModelSerializer):
    class Meta:
        model = Answers
        fields = ('id', 'user_name', 'create_time', 'title', 'q_content', 'author_name', 'a_content',)

    # 问题详情的字段
    # fields = ('id', 'user_name', 'create_time', 'title', 'q_content', 'author_name', 'a_content', )


class QuestionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questions
        fields = ('id', 'title',)


class TagsListSerializers(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = ('id', 'name', 'people')


class TagsDetailSerializers(serializers.ModelSerializer):
    # answers = AnswersListSerializers()

    class Meta:
        model = Tags
        fields = ('id', 'name', 'people', 'image', 'abstract',)


class CategoryIdSerializer(serializers.ModelSerializer):
    class Meta:
        model = CategoryId
        fields = ('id', 'name', )

