from django.shortcuts import render
from rest_framework.generics import CreateAPIView, DestroyAPIView
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet

from spit.serialzers import SpitListSerialzer, SpitLikeSerialezer, SpitCollectionSerializer, SpitCommentSerializer, \
    SpitCommentLikeSerializer
from spit.utils import StandardResultsSetPagination
from .models import Spit, SpitLike, SpitCollection, SpitComment, SpitCommentLike


# Create your views here.

class SpitViewSet(ModelViewSet):

    serializer_class = SpitListSerialzer
    queryset = Spit.objects.all()
    pagination_class = StandardResultsSetPagination


    def list(self,request,*args,**kwargs):
        # 获取用户信息
        user = request.user
        user_likes_list = [] # 用户点赞
        user_collections_list = []  # 用户收藏


        # 判断用户是否登陆
        if user is not None:
            # 获取用户点赞记录
            user_likes = SpitLike.objects.filter(user=user.id).only('spit')
            for user_like in user_likes:
                user_likes_list.append(user_like.spit.id)
            # 获取用户的收藏记录
            user_collections = SpitCollection.objects.filter(user=user.id).only('spit')
            for user_collection in user_collections:
                user_collections_list.append(user_collection.spit.id)
        # 分页
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            queryset = page
        else:
            queryset = self.queryset

        serializer = self.get_serializer(self.get_queryset(), many=True)
        # 点赞1
        for spit in serializer.data:
            # 判断是否点赞
            if spit['id'] in user_likes_list:
                spit['is_like'] =True
            else:
                spit['is_like'] = False
            # 判断是否收藏
            if spit['id'] in user_collections_list:
                spit['is_collection'] = True
            else:
                spit['is_collection'] = False
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        """详情"""
        user = request.user
        user_likes_list = []  # 用户点赞
        user_collections_list = []  # 用户收藏

        # 判断用户是否登陆
        if user is not None:
            # 获取用户点赞记录
            user_likes = SpitLike.objects.filter(user=user.id).only('spit')
            for user_like in user_likes:
                user_likes_list.append(user_like.spit.id)
            # 获取用户的收藏记录
            user_collections = SpitCollection.objects.filter(user=user.id).only('spit')
            for user_collection in user_collections:
                user_collections_list.append(user_collection.spit.id)
        queryset = self.get_object()
        serializer = self.get_serializer(queryset)
        # 判断是否点赞
        data = dict(serializer.data)
        if data['id'] in user_likes_list:
            data['is_like'] = True
        else:
            data['is_like'] = False
        # 判断是否收藏
        if data['id'] in user_collections_list:
            data['is_collection'] = True
        else:
            data['is_collection'] = False
        return Response(data)

class SpitLikeView(CreateAPIView,DestroyAPIView):
    """点赞吐槽"""
    serializer_class = SpitLikeSerialezer
    queryset = SpitLike.objects.all()

    def post(self,request,*args,**kwargs):
        """点赞"""
        try:
            print(request.data)
            respone = super().post(request,*args,**kwargs)
            spit = request.data['spit']
            sp = Spit.objects.get(id=spit)
            sp.like_count +=1
            sp.save()
        except Exception as e:
            raise e
        else:
            return respone

    def destroy(self,request,spit,*args,**kwargs):
        """取消点赞"""
        spit_like = self.queryset.filter(spit=spit,user=request.user.id)
        # 如过记录存在
        if spit_like:
            spit_like.delete()
            # 对应相关吐槽点赞数-1
            sp = Spit.objects.get(id = spit)
            sp.like_count -= 1
            sp.save()
            return Response(status=200)
        return Response(status=400)

class SpitCollectionView(CreateAPIView, DestroyAPIView):
    """
    吐槽收藏
    """
    serializer_class = SpitCollectionSerializer
    queryset = SpitCollection.objects.all()

    def post(self, request, *args, **kwargs):
        """
        点赞
        """
        try:
            respone = super().post(request, *args, **kwargs)
            spit = request.data['spit']
            sp = Spit.objects.get(id=spit)
            sp.collecton_count += 1
            sp.save()
        except Exception as e:
            raise e
        else:
            return respone

    def destroy(self, request, spit, *args, **kwargs):
        """
        取消收藏
        """
        spit_collection = self.queryset.filter(spit=spit, user=request.user.id)
        # 如过记录存在
        if spit_collection:
            spit_collection.delete()
            # 对应相关吐槽点赞数-1
            sp = Spit.objects.get(id=spit)
            sp.collecton_count -= 1
            sp.save()
            return Response(status=200)
        return Response(status=400)

class SpitCommentView(CreateAPIView):
    """
    吐槽评论
    """
    serializer_class = SpitCommentSerializer
    # queryset = SpitComment.objects.all().order_by("-create_time")
    queryset = SpitComment.objects.all()
    # pagination_class = StandardResultsSetPagination



    def get(self, request, spit):
        # 获取用户信息
        user = request.user
        user_likes_list = []  # 用户点赞
        # 判断用户是否登录

        if user is not None:
            user_likes = SpitCommentLike.objects.filter(user=user.id).only('comment')
            for user_like in user_likes:
                user_likes_list.append(user_like.comment.id)
        queryset = self.get_queryset().filter(spit=spit)
        # 分页
        # page = self.paginate_queryset(queryset)
        # if page is not None:
        #     queryset = page

        serializer = self.get_serializer(queryset, many=True)
        # 添加是否点赞，是否收藏字段
        for spit in serializer.data:
            # 判断是否点赞
            if spit['id'] in user_likes_list:
                spit['is_like'] = True
            else:
                spit['is_like'] = False

        return Response(serializer.data)

class SpitCommentLikeView(CreateAPIView, DestroyAPIView):
    """
    吐槽评论点赞
    """
    serializer_class = SpitCommentLikeSerializer
    queryset = SpitCommentLike.objects.all()

    # 点赞,,相关评论数+1
    def post(self, request, *args, **kwargs):
        print(request.data)
        try:
            respone = super().post(request, *args, **kwargs)
            comment = request.data['comment']
            comm = SpitComment.objects.get(id=comment)
            comm.like_count += 1
            comm.save()
        except Exception as e:
            raise e
        else:
            return respone

    def destroy(self, request, spitcomment, *args, **kwargs):
        """
        取消评论点赞
        """

        spit_comment_like = self.queryset.filter(comment=spitcomment, user=request.user.id)
        if spit_comment_like:
            spit_comment_like.delete()

            # 取消点赞,相关评论数-1
            comm = SpitComment.objects.get(id=spitcomment)
            comm.like_count -= 1
            comm.save()
            return Response(status=200)
        return Response(status=400)