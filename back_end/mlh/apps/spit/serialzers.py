from rest_framework import serializers

from spit.models import Spit, SpitLike, SpitCollection, SpitComment, SpitCommentLike
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    # avatar = serializers.CharField()
    class Meta:
        model = User
        # fields = ('id', 'username','avatar' )
        fields = ('id', 'username' )


class SpitListSerialzer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)

    class Meta:
        model = Spit
        fields = ('id', 'content','author', 'like_count', 'collecton_count','comment_count','create_time')
        extra_kwargs = {
            'like_count':{'read_only':True},
            'collecton_count':{'read_only':True},
            'comment_count':{'read_only':True},
        }
    def create(self,validated_data):
        author_id = self.context['request'].user.id
        validated_data['author_id'] = author_id
        return super().create(validated_data)

class SpitLikeSerialezer(serializers.ModelSerializer):
    class Meta:
        model = SpitLike
        fields = ('id','spit')

    # 获取用户登陆信息
    def create(self, validated_data):
        print(self.context)
        validated_data['user'] = self.context['request'].user
        return  super().create(validated_data)
    def validate_spit(self, value):
        """校验所属吐槽"""
        if not Spit.objects.get(id =value.id):
            raise serializers.ValidationError('内容不存在')
        return value

class SpitCollectionSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpitCollection
        fields = ('id','spit',)

    # 获取用户登陆信息
    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)

    def validate_spit(self, value):
        """校验所属吐槽"""
        if not Spit.objects.get(id=value.id):
            raise serializers.ValidationError('内容不存在')
        return value

class SpitCommentSerializer(serializers.ModelSerializer):
    """
        吐槽评论序列化器
        """
    spit_id = serializers.IntegerField()
    author = UserSerializer(read_only=True)

    class Meta:
        model = SpitComment
        fields = ('id', 'content', 'spit_id', 'create_time', 'like_count', 'author',)
        extra_kwargs = {
            'like_count': {
                'read_only': True
            },
            'create_time': {
                'read_only': True
            },
            'spit_id': {
                'write_only': True
            }
        }

    def create(self, validated_data):
        print(self.context['request'].user)
        validated_data['author_id'] = self.context['request'].user.id
        try:
            resp = super().create(validated_data)
            spit = validated_data['spit_id']

            # 添加评论,评论数+1
            sp = Spit.objects.get(id=spit)
            sp.comment_count += 1
            sp.save()
        except Exception as e:
            raise e
        else:
            return resp


class SpitCommentLikeSerializer(serializers.ModelSerializer):
    """
        吐槽评论点赞序列化器
        """

    class Meta:
        model = SpitCommentLike
        fields = ('comment',)

    # 获取用户登录的相关信息
    def create(self, validated_data):
        print(validated_data)
        validated_data['user'] = self.context['request'].user

        return super().create(validated_data)

    def validate_comment(self, value):
        """校验所属评论"""
        print(value)
        if not SpitComment.objects.get(id=value.id):
            print(1)
            raise serializers.ValidationError('吐槽内容不存在')
        print(2)
        return value
