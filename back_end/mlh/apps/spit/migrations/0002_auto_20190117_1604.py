# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2019-01-17 08:04
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('spit', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='spitlike',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户'),
        ),
        migrations.AddField(
            model_name='spitcommentlike',
            name='comment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='spit.SpitComment', verbose_name='所属吐槽'),
        ),
        migrations.AddField(
            model_name='spitcommentlike',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户'),
        ),
        migrations.AddField(
            model_name='spitcomment',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='spit_comments', to=settings.AUTH_USER_MODEL, verbose_name='作者'),
        ),
        migrations.AddField(
            model_name='spitcomment',
            name='spit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='comments', to='spit.Spit', verbose_name='所属吐槽'),
        ),
        migrations.AddField(
            model_name='spitcollection',
            name='spit',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='spit.Spit', verbose_name='所属吐槽'),
        ),
        migrations.AddField(
            model_name='spitcollection',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='用户'),
        ),
        migrations.AddField(
            model_name='spit',
            name='author',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='spit', to=settings.AUTH_USER_MODEL, verbose_name='作者'),
        ),
        migrations.AlterUniqueTogether(
            name='spitlike',
            unique_together=set([('user', 'spit')]),
        ),
        migrations.AlterUniqueTogether(
            name='spitcommentlike',
            unique_together=set([('user', 'comment')]),
        ),
        migrations.AlterUniqueTogether(
            name='spitcollection',
            unique_together=set([('user', 'spit')]),
        ),
    ]
