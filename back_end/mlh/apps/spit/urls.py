from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from . import views


urlpatterns = [
    url(r'^spit/$',views.SpitViewSet.as_view({"get":"list","post":"create"})),
    # url(r'^spit/$',views.SpitViewSet.as_view({"post":"create"})),
    url(r'^spit/(?P<pk>\d+)$',views.SpitViewSet.as_view({"get":"retrieve"})),
    url(r'^spit/likes/$',views.SpitLikeView.as_view()),
    url(r'^spit/likes/(?P<spit>\d+)/$',views.SpitLikeView.as_view()),
    url(r'^spit/collections/$', views.SpitCollectionView.as_view()),
    url(r'^spit/collections/(?P<spit>\d+)/$', views.SpitCollectionView.as_view()),
    url(r'^spit/(?P<spit>\d+)/comments/$', views.SpitCommentView.as_view()),  # 吐槽评论&增加和查找
    url(r'^spit/commentlike/$', views.SpitCommentLikeView.as_view()),  # 吐槽评论点赞
    url(r'^spit/commentlikes/(?P<spitcomment>\d+)/$', views.SpitCommentLikeView.as_view()),  # 取消评论点赞


]
