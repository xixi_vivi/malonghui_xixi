from django.db import models

# Create your models here.
from users.models import User


class Base(models.Model):
    create_time = models.DateTimeField(auto_now_add=True,verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True,verbose_name="更新时间")

    class Meta:
        # 抽象类，不迁移
        abstract = True



class Spit(Base):
    content = models.TextField(verbose_name="内容")
    author = models.ForeignKey(User,on_delete=models.CASCADE,related_name="spit",verbose_name="作者")
    like_count = models.IntegerField(verbose_name="点赞数",default=0)
    share_count = models.IntegerField(verbose_name="分享数",default=0)
    collecton_count = models.IntegerField(verbose_name="收藏数",default=0)
    comment_count = models.IntegerField(verbose_name="评论数",default=0)
    create_time = models.DateTimeField(auto_now_add=True,verbose_name="创建时间")


    def __str__(self):
        return self.content[:30]

    class Meta:
        db_table = 'tb_spit'
        verbose_name = '吐槽'
        verbose_name_plural=verbose_name

class SpitLike(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE,verbose_name="用户")
    spit = models.ForeignKey(Spit,on_delete=models.CASCADE,verbose_name="吐槽")

    class Meta:
        db_table = "tb_spit_likes"
        verbose_name = "点赞吐槽"
        verbose_name_plural = verbose_name
        unique_together = ('user','spit') # 联合主键

class SpitCollection(models.Model):
    """
    吐槽收藏
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="用户")
    spit = models.ForeignKey(Spit, on_delete=models.CASCADE, verbose_name="所属吐槽")

    class Meta:
        db_table = "tb_spit_collections"
        verbose_name = "吐槽收藏"
        verbose_name_plural = verbose_name
        unique_together = ('user', 'spit')

class SpitComment(Base):
    """
    吐槽评论
    """
    content = models.TextField(verbose_name="内容")
    spit = models.ForeignKey(Spit, on_delete=models.CASCADE, related_name="comments",
                                  verbose_name="所属吐槽")
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name="spit_comments", verbose_name="作者")
    like_count = models.IntegerField(verbose_name="点赞数", default=0)

    class Meta:
        db_table = "tb_spit_comments"
        verbose_name = "吐槽评论"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.content

class SpitCommentLike(models.Model):
    """
    吐槽评论点赞
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="用户")
    comment = models.ForeignKey(SpitComment, on_delete=models.CASCADE, verbose_name="所属吐槽")

    class Meta:
        db_table = "tb_spit_comment_likes"
        verbose_name = "吐槽评论点赞"
        verbose_name_plural = verbose_name
        unique_together = ('user', 'comment')
