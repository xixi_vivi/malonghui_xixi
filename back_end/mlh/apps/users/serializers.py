import re

from django_redis import get_redis_connection
from rest_framework import serializers
from rest_framework_jwt.settings import api_settings

from mlh.utils.models import BaseModel
from qa.models import Answers, Questions
from .models import User


class CreateUserSerializer(serializers.ModelSerializer):
    """
    创建用户的序列化器
    """
    password = serializers.CharField(label='确认密码', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(label='JWT token', read_only=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'password',  'sms_code', 'mobile', 'allow', 'token')
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate_mobile(self, value):
        """验证手机号"""
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def validate_allow(self, value):
        """检验用户是否同意协议"""
        if value != 'true':
            raise serializers.ValidationError('请同意用户协议')
        return value

    def validate(self, data):

        # 判断短信验证码
        redis_conn = get_redis_connection('default')
        mobile = data['mobile']
        real_sms_code = redis_conn.get('sms_%s' % mobile)
        if real_sms_code is None:
            raise serializers.ValidationError('无效的短信验证码')
        if data['sms_code'] != real_sms_code.decode():
            raise serializers.ValidationError('短信验证码错误')

        return data

    def create(self, validated_data):
        """
       创建用户 重写保存方法,增加密码加密
       """
        # 移除数据库模型类中不存在的属性
        del validated_data['sms_code']
        del validated_data['allow']

        # user = super.create(validated_data)

        user = User.objects.create(**validated_data)

        # 调用django的认证系统加密密码
        user.set_password(validated_data['password'])
        user.save()

        # 签发jwt
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)
        user.token = token
        return user

class ImageCodeCheckSerializer(serializers.Serializer):
    """
    短信验证码校验序列化器
    """

    def validate(self, attrs):
        """
        校验
        """
        # 判断是否在60s内
        # get_serializer 方法在创建序列化器的时候,序列化器对象会补充context属性
        # context属性中包含三个值 request format view 类视图对象
        # self.context['view']
        # django的类视图对象中,kwargs保存了路径提取出来的参数
        redis_conn = get_redis_connection('default')
        mobile = self.context['view'].kwargs['mobile']
        send_flag = redis_conn.get("send_flag_%s" % mobile)
        if send_flag:
            raise serializers.ValidationError('请求次数过于频繁')

        return attrs


class UserDetailSerializer(serializers.ModelSerializer):
    """
    用户详细信息序列化器
    """
    password = serializers.CharField(label='确认密码', write_only=True)
    # user_url = serializers.CharField(label='个性网址',read_only=True)

    class Meta:
        model = User
        fields = '__all__'
        # fields = ('username','mobile','email','user_url','password','now_city','school','company','abstract')
        # extra_kwargs = {
        #     "password": {
        #         "write_only": True
        #     }
        # }

class UserEditSerializer(serializers.ModelSerializer):
    """编辑用户序列化器"""
    # mobile = serializers.StringRelatedField(max_length=11,min_length=11, unique=True, verbose_name='手机号')

    class Meta:
        model = User
        fields = '__all__'
        extra_kwargs = {
            "password": {
                "write_only": True
            }
        }

    # mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    # user_url = models.CharField(max_length=128, null=True, unique=True, verbose_name='个性网址')
    # school = models.CharField(max_length=16, null=True, blank=True, verbose_name='毕业院校')
    # company = models.CharField(max_length=32, null=True, blank=True, verbose_name='公司')
    # now_city = models.CharField(max_length=32, null=True, blank=True, verbose_name='现居城市')
    # birthday = models.DateField(verbose_name='生日', null=True)
    # gender = models.SmallIntegerField(choices=((0, 'male'), (1, 'female')), verbose_name='性别', null=True)
    # address = models.CharField(max_length=64, null=True, blank=True, verbose_name='通信地址')

    # def create(self, validated_data):
    #     """保存"""
    #     validated_data['user'] = self.context['request'].user
    #     return super().create(validated_data)


class AddressTitleSerializer(serializers.ModelSerializer):
    """
    地址标题
    """
    class Meta:
        model = User
        fields =  '__all__'

class UserQuestionSerializer(serializers.ModelSerializer):
    """问题序列化器"""
    class Meta:
        model = Questions
        fields = ('title', 'like_count')
#
#
class UserAnswerSerializer(serializers.ModelSerializer):
    """用户回答问题序列化器"""
    question = UserQuestionSerializer(read_only=True)

    class Meta:
        model = Answers
        fields = ('question', 'update_time')



# class UserMyquestserializer(serializers.ModelSerializer):
#     """我的问题序列化器"""
#     create_time = BaseModel()
#
#     class Meta:
#         model = Questions
#         fields = ('create_time', "like_count", "title", 'id')