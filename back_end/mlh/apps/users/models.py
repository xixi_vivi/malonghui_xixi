from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.


class User(AbstractUser):
    """用户模型类"""
    mobile = models.CharField(max_length=11, unique=True, verbose_name='手机号')
    id = models.AutoField(verbose_name='用户id', primary_key=True)
    # user_url = models.CharField(max_length=128, null=True, unique=True, verbose_name='个性网址')
    # school = models.CharField(max_length=16,null=True,blank=True,verbose_name='毕业院校')
    # company = models.CharField(max_length=32,null=True,blank=True,verbose_name='公司')
    # now_city = models.CharField(max_length=32,null=True,blank=True,verbose_name='现居城市')
    # birthday = models.DateField(verbose_name='生日', null=True)
    # gender = models.SmallIntegerField(choices=((0, 'male'), (1, 'female')), verbose_name='性别', null=True)
    # address = models.CharField(max_length=64,null=True,blank=True,verbose_name='通信地址')
    user_url = models.CharField(max_length=128, null=True, unique=True, verbose_name='个性网址')
    school = models.CharField(max_length=16,null=True,blank=True,verbose_name='毕业院校')
    company = models.CharField(max_length=32,null=True,blank=True,verbose_name='公司')
    now_city = models.CharField(max_length=32,null=True,blank=True,verbose_name='现居城市')
    birthday = models.DateField(verbose_name='生日', null=True)
    gender = models.SmallIntegerField(choices=((0, 'male'), (1, 'female')), verbose_name='性别', null=True)
    address = models.CharField(max_length=64,null=True,blank=True,verbose_name='通信地址')


    # abstract = models.CharField(max_length=128,null=True,blank=True,verbose_name='个人简介')
    # image = models.CharField(max_length=128, null=True, blank=True, verbose_name="头像")

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username

