import logging
import random
import re

from django.contrib.auth.backends import ModelBackend
from rest_framework import mixins
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework_jwt.views import ObtainJSONWebToken
from celery_tasks.sms.tasks import send_sms_code
from django_redis import get_redis_connection
from rest_framework.generics import GenericAPIView, CreateAPIView, RetrieveAPIView, UpdateAPIView, ListAPIView
from rest_framework.response import Response
from celery_tasks.sms import tasks as sms_tasks

from qa.models import Answers
from spit.models import Spit
from users import constants, serializers
from users.serializers import UserAnswerSerializer
from .models import User
# Create your views here.


logger = logging.getLogger('django')


# url('^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
class SMSCodeView(GenericAPIView):
    """
    短信验证码
    传入参数：
        mobile, tex
    """
    serializer_class = serializers.ImageCodeCheckSerializer

    def get(self, request, mobile):
        # 校验参数  由序列化器完成,判断是否在60s内
        serializer = self.get_serializer(data=request.query_params)
        serializer.is_valid(raise_exception=True)
        # 生成短信验证码
        sms_code = '%06d' % random.randint(0, 999999)
        print("短信验证码为 ", sms_code)
        # 保存短信验证码与发送记录
        redis_conn = get_redis_connection('default')
        # redis管道
        pl = redis_conn.pipeline()
        pl.setex("sms_%s" % mobile, constants.SMS_CODE_REDIS_EXPIRES, sms_code)
        pl.setex("send_flag_%s" % mobile, constants.SEND_SMS_CODE_INTERVAL, 1)
        # 让管道通知redis执行命令
        pl.execute()

        expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)

        send_sms_code.delay(mobile, sms_code, expires, constants.SMS_CODE_TEMP_ID)
        return Response({"message": "OK"})
        # # 发送短信验证码
        # sms_code_expires = str(constants.SMS_CODE_REDIS_EXPIRES // 60)
        # sms_tasks.send_sms_code.delay(mobile, sms_code, sms_code_expires)
        #
        # return Response({"message": "OK"})



# url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
class UsernameCountView(APIView):
    """
    用户名数量
    """
    def get(self, request, username):
        """
        获取指定用户名数量
        """
        count = User.objects.filter(username=username).count()
        data = {
            'username': username,
            'count': count
        }
        return Response(data)


# url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
class MobileCountView(APIView):
    """
    手机号数量
    """
    def get(self, request, mobile):
        """
        获取指定手机号数量
        """
        count = User.objects.filter(mobile=mobile).count()

        data = {
            'mobile': mobile,
            'count': count
        }

        return Response(data)


# url(r'^users/$', views.UserView.as_view()),
class UserView(CreateAPIView):
    """
    用户注册
    传入参数：
       username, password, password2, sms_code, mobile, allow
    """
    serializer_class = serializers.CreateUserSerializer


class UserAuthorizeView(ObtainJSONWebToken):
    """
    用户认证
    """
    def post(self, request, *args, **kwargs):
        # 调用父类的方法，获取drf jwt扩展默认的认证用户处理结果
        response = super().post(request, *args, **kwargs)
        return response


# 访问视图必须要求用户已通过认证（即登录之后）
# GET /user/detail/
class UserDetailView(RetrieveAPIView):
    """用户信息详情"""
    # 查询用户数据  序列化返回  继承了RetrieveAPIView无需get
    # 声明序列化器类
    serializer_class = serializers.UserDetailSerializer
    # 设置权限[仅供登陆认证用户]
    permission_classes = [IsAuthenticated]

    def get_object(self):
        # 返回当前请求的用户对象
        # 在类师徒对象中,可以通过类视图对象的属性获取request
        # 在django的请求request对象中,user属性表明当前请求的用户
        return self.request.user

# patch /user/detail/
class UserEditView(UpdateAPIView):
    """用户编辑"""
    permission_classes = [IsAuthenticated]
    serializer_class = serializers.UserEditSerializer
    queryset = User.objects.all()


# GET /user/answers
class UserAnswer(ListAPIView):
    """用户回答的问题"""
    # 返回用户回答的问题(五条数据，按照时间倒序排序)
    # 数据格式 [{"title": 问题的标题, "like_count": 问题点赞的数量, "update_time": 用户回答问题的时间}, {}, {}]
    # 查询用户所有回答的问题 answer = Answer.objects.get(respondent_id=user_id)  查询字符串形式获取用户id
    # 序列化返回
    serializer_class = UserAnswerSerializer

    def get_queryset(self):
        user_id = self.kwargs['pk']
        return Answers.objects.filter(respondent_id=user_id)[0:5]


# class UserMyquestionView(ListAPIView):
#     """
#     我的提问
#     """
    # serializer_class = serializers.UserMyquestserializer
    #
    # def get_queryset(self):
    #     user_id = self.kwargs.get('user_id')
    #     # print(" ——————————%s" %user_id)
    #     return Spit.objects.filter(author=user_id)

