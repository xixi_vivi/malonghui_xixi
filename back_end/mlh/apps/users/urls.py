from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token

from . import views


urlpatterns = [
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    url(r'^users/$', views.UserView.as_view()),
    url(r'^sms_codes/(?P<mobile>1[3-9]\d{9})/$', views.SMSCodeView.as_view()),
    # url(r'^authorizations/$', views.UserAuthorizeView.as_view()),  # 登录认证,补充合并购物车
    url(r'^authorizations/$', obtain_jwt_token),  # 登录认证
    url(r'^user/detail/$', views.UserDetailView.as_view()),  # 个人详细信息
    url(r'^edit/$', views.UserEditView.as_view()),  # 个人详细信息
    url(r'^user/answers/(?P<pk>\d+)/$', views.UserAnswer.as_view()),
    # url(r'^usermyquestion/(?P<user_id>\d+)/$', views.UserMyquestionView.as_view()),  # 用户页面修改,
    # url(r'^browse_histories/$', views.UserBrowsingHistoryView.as_view())
    # url(r'^userdetail/(?P<user_id>\d+)/$', views.UserDetailAPIView.as_view()),  # 用户详情
    # url(r'^userdetails/(?P<pk>\d+)/$', views.UserChangeDetailAPIView.as_view()),
]


# router = routers.DefaultRouter()
# router.register(r'edit', views.UserEditViewSet, base_name='edit')
#
# urlpatterns += router.urls