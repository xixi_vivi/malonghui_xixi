from django.contrib import admin
from . import models

# Register your models here.
admin.site.register(models.Category)
admin.site.register(models.News)
admin.site.register(models.Comment)

class CategoryAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = True
#
#     list_display = ['name', 'index']
#
#     def pub_date(self):
#         return self.bpub_date.strftime('%Y年%m月%d日')
#
#     pub_date.short_description = '分类排序'
#     pub_date.admin_order_field = 'index'

class NewsAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = True


class CommentAdmin(admin.ModelAdmin):
    actions_on_top = True
    actions_on_bottom = True