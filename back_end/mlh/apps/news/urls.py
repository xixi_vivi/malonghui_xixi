from django.conf.urls import url
from . import views
from rest_framework.routers import DefaultRouter

urlpatterns = [
    url(r'^index/category/$', views.CategoryViewSet.as_view()),
    # url(r'^index/(?P<cid>[0-9])/$', views.NewsViewSet.as_view({'get': 'list'})),
    url(r'^index/$', views.NewsViewSet.as_view({'get': 'list'})),
    url(r'^news/(?P<pk>\d+)/$', views.NewsViewSet.as_view({'get': 'retrieve'})),
    url(r'^news/publish/$', views.PublishNewsViewSet.as_view()),
    url(r'^news/comment/(?P<pk>\d+)/$', views.CommentViewSet.as_view()),
]

# router = DefaultRouter()
# router.register(r'news/comment', views.CommentViewSet, base_name="comment")
# urlpatterns += router.urls
