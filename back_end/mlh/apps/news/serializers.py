from rest_framework import serializers

from users.models import User
from .models import Category, News, Comment


# 评论嵌套序列化器
class AuthorSerializer(serializers.ModelSerializer):
    image = serializers.CharField(label="头像", read_only=True)

    class Meta:
        model = User
        fields = ("id", "username", "image")


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ("id", "name")


class NewsListSerializer(serializers.ModelSerializer):
    author = AuthorSerializer()

    class Meta:
        model = News
        fields = ("id", "category", "title", "author", "create_time", "image_url", "abstract", "clicks")


class NewsDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = News
        exclude = ("is_delete", "abstract", "image_url")


class CommentSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(label="id", min_value=1, read_only=True)
    parent_id = serializers.IntegerField(label="所属评论", min_value=1, read_only=True)
    content = serializers.CharField(label="评论内容", min_length=1, max_length=256)
    remark_count = serializers.IntegerField(read_only=True)
    com_author = AuthorSerializer(read_only=True)

    class Meta:
        model = Comment
        fields = ("id", "remark_count", "parent_id", "content", "com_author")


class SubCommentSerializer(serializers.ModelSerializer):
    sub = CommentSerializer(many=True, read_only=True)

    class Meta:
        model = CommentSerializer
        fields = ("sub", "id", "com_author", "remark_count", "parent_id", "content")



class PublishNewsSerializer(serializers.ModelSerializer):
    """发布头条序列化器"""
    title = serializers.CharField(label='标题',min_length=1,write_only=True)
    author_id = serializers.IntegerField(label='作者id')

    class Meta:
        model = News
        fields = ('title','content','category','category_id','author_id')

# class ColumnSerializer(serializers.ModelSerializer):
#     """申请专栏"""
    # 可以不写

    # class Meta:
    #     model =
    # column
    # 专栏名称
    # col_url
    # 网址
    # summary
    # 简介