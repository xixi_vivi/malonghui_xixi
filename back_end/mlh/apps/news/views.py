from django.shortcuts import render
from rest_framework import status
from rest_framework.generics import GenericAPIView, ListCreateAPIView, ListAPIView, CreateAPIView
from rest_framework.mixins import RetrieveModelMixin
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ReadOnlyModelViewSet, ModelViewSet

from news.serializers import CommentSerializer, SubCommentSerializer,PublishNewsSerializer
from .models import Category, News, Comment
from . import serializers


class CategoryViewSet(ListAPIView):
    """分类视图"""
    queryset = Category.objects.all()
    serializer_class = serializers.CategorySerializer


class NewsViewSet(ReadOnlyModelViewSet):
    """新闻视图"""
    def get_queryset(self):
        # 如果访问list
        if self.action == "list":
            # 获取前端传递的分类id　返回新闻列表
            cid = self.request.query_params.get("cid")
            # 如果访问0就是热门，查询出点击量显示
            if cid == "0":
                return News.objects.filter(is_delete=False).order_by("-clicks").select_related()
            else:
                return News.objects.filter(is_delete=False, category_id=cid).order_by("-create_time").select_related()
        else:
            # 不是则返回新闻详情
            return News.objects.all()

    def get_serializer_class(self):
        if self.action == "list":
            # 返回新闻列表
            return serializers.NewsListSerializer
        else:
            # 新闻详情
            return serializers.NewsDetailSerializer


# class NewsView(GenericAPIView):
#
#     def get(self, request):
#         news = News.objects.get(id=1)
#         serializer = serializers.NewsDetailSerializer(news)
#         return Response(serializer.data)

class CommentViewSet(ListCreateAPIView):
    """评论"""
    # queryset = Comment.objects.all()
    def get_queryset(self):
            news_id = self.kwargs.get("pk")
            return Comment.objects.filter(is_delete=False, news=news_id).select_related()


    serializer_class = CommentSerializer


    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(CommentSerializer(serializer.instance).data, status=status.HTTP_201_CREATED,
                        headers=headers)


# # url(r'^news/(?P<pk>\d+)/$'),
# class NewsDetailView(RetrieveAPIView):
#     queryset = News.objects.all()
#     serializer_class = NewsDetailSerializer

# http://{host}/news/publish/
class PublishNewsViewSet(CreateAPIView):
    """发布头条"""
    permission_classes = [IsAuthenticated] # 权限验证
    serializer_class = PublishNewsSerializer


# class ColumnViewSet(CreateAPIView):
#     """申请专栏"""
#     serializer_class = ColumnSerializer
