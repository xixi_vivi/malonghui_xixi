from django.db import models
from mlh.utils.models import BaseModel
from users.models import User
from mlh.utils.models import BaseModel


class Category(models.Model):
    """新闻分类"""
    name = models.CharField(max_length=12, verbose_name="分类名称")
    index = models.SmallIntegerField(default=1, verbose_name="分类排序")
    is_delete = models.BooleanField(default=0, verbose_name="删除")

    class Meta:
        db_table = "news_category"
        verbose_name = "新闻分类"
        verbose_name_plural = verbose_name

    def __str__(self):
        return "%s" % self.name


class News(BaseModel):
    """新闻头条"""
    # category = models.ForeignKey(Category, related_name="cid", verbose_name="分类")
    # author = models.ForeignKey(User, verbose_name="新闻作者")
    # models.ForeignKey("self", blank=True, null=True, related_name='location_parent')
    category = models.ForeignKey(Category, null=True,related_name="cid", verbose_name="分类")
    author = models.ForeignKey(User, on_delete=models.PROTECT,null=True, verbose_name="新闻作者")
    title = models.CharField(max_length=64, verbose_name="新闻标题")
    abstract = models.CharField(max_length=256, verbose_name="新闻摘要")
    clicks = models.IntegerField(default=0, verbose_name="点击量")
    content = models.TextField(default="", verbose_name="正文")
    # user_collection = models.IntegerField(blank=True, null=True, verbose_name="文章收藏")
    image_url = models.CharField(max_length=128, verbose_name="配图", blank=True, null=True)
    is_delete = models.BooleanField(default=False, verbose_name="删除")

    class Meta:
        db_table = "news"
        verbose_name = "新闻头条"
        verbose_name_plural = verbose_name

    def __str__(self):
        return "%s: %s" % (self.category.name, self.title)


class Comment(BaseModel):
    """评论"""
    # com_author = models.ForeignKey(User, on_delete=models.PROTECT, related_name="comm", verbose_name="评论用户")
    # news = models.ForeignKey(News, on_delete=models.CASCADE, related_name="news_comm", verbose_name="所属新闻")
    # parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True, verbose_name="父评论")
    com_author = models.ForeignKey(User, on_delete=models.PROTECT, default='',related_name="comm", verbose_name="评论用户")
    news = models.ForeignKey(News, on_delete=models.CASCADE,null=True, related_name="news_comm", verbose_name="所属新闻")
    parent = models.ForeignKey("self", on_delete=models.SET_NULL, null=True, blank=True,verbose_name="父评论")
    content = models.TextField(max_length=128, verbose_name="评论内容")
    remark_count = models.IntegerField(default=0, verbose_name="回复数")
    is_delete = models.BooleanField(default=False, verbose_name="删除")

    class Meta:
        db_table = "comment"
        verbose_name = "评论"
        verbose_name_plural = verbose_name
        ordering = ("-create_time",)

    # def __str__(self):
    #     return "%s" % self.com_author
