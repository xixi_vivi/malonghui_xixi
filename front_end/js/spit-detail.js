/**
 * Created by python on 19-1-11.
 */
// axios.get("http://127.0.0.1:8000/spit/")
// .then(response=>{
//     // console.log(response)
// })
// .catch(error =>{})

var app = new Vue({
    el: "#left-list",
    data: {
        // 页面中需要使用到的数据 键值对
        spit: null,
        token: sessionStorage.token || localStorage.token || '',
        username: sessionStorage.username || localStorage.username || ' ',
        user_id: sessionStorage.user_id || localStorage.user_id || ' ',
        host,
        is_like: false,
        is_collection: false,
        spit_id:0,
        headers:{},
        comments:[],
        show_comment_post:false,// 是否展示增加评论的框
        new_comment_content:"",// 新增评论的内容

    },
    computed: {
        // 需要通过计算得到的数据 键值对 ,键是名称,值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的   直接是代码
        this.spit_id = get_query_string('id')
        //  向后端发送请求获取吐槽信息，首先判断是否存在toke
        if(this.token){
            this.headers = {
                'Authorization': 'JWT ' + this.token
                // 'Authorization': '' + this.token
                        }
        }
        console.log("哈哈"+this.headers)
        axios.get(host + "/spit/" + this.spit_id ,{
                headers: this.headers,
                responseType: 'json'
            })
            .then(response => {
                // console.log(response)
                this.spit = response.data
            })
            .catch(error => {
            })
        // 获取吐槽评论列表
         axios.get(host + '/spit/'+ this.spit_id +'/comments/',
            {
                headers: this.headers,
                responseType: 'json'
            })
            .then(response =>{
                // console.log(response.data);
                this.comments = response.data
            })
            .catch(error => {

            })

    },
    methods: {
        //需要用到的函数  键值对 ,键是名称,值是匿名函数
        // 获取url路径参数
        get_query_string: function (key) {
            var url = document.location.toString()
            var arrUrl = url.split(key+'=')
            if (arrUrl.length>1){
                var value = arrUrl[1].split("&")
                return  value[0]
            }else{
                return null
            }

        },
        // 获取评论列表
        get_comment: function () {
            alert(1)
            axios.get(host + '/spit/'+ this.spit_id +'/comments/',
            {
                headers: this.headers,
                responseType: 'json'
            })
            .then(response =>{
                // console.log(response.data);
                this.comments = response.data
            })
            .catch(error => {

            })
        },

        // 吐槽点赞
        spit_like: function () {
            //如果用户没有登录
            if (!this.token) {
                location.href = '/person-loginsign.html?next=/spit-detail.html?id='+this.spit_id
                return
            }
            axios.post(host + '/spit/likes/',
                {
                    spit: this.spit_id
                },
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response => {

                    this.spit.is_like = true
                    this.spit.like_count += 1

                })
                .catch(error => {

                })
        },
        // 吐槽取消点赞
        remove_spit_like: function () {
            if (!this.token) {
                location.href = '/person-loginsign.html?next=/spit-detail.html?id=' + this.spit_id
                return
            }
            axios.delete(host + '/spit/likes/' + this.spit_id + '/',

                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response => {

                    this.spit.is_like = false
                    this.spit.like_count -= 1;
                })
                .catch(error => {

                })
        },
        // 吐槽收藏
        spit_collection: function () {
            //如果用户没有登录
            if (!this.token) {
                location.href = '/person-loginsign.html?next=/spit-detail.html?id='+this.spit_id
                return
            }
            axios.post(host + '/spit/collections/',
                {
                    spit: this.spit_id
                },
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response => {

                    this.spit.is_collection = true
                    this.spit.collecton_count += 1

                })
                .catch(error => {

                })
        },
        // 吐槽取消收藏
        remove_spit_collection: function () {
            if (!this.token) {
                location.href = '/person-loginsign.html?next=/spit-detail.html?id=' + this.spit_id
                return
            }
            axios.delete(host + '/spit/collections/' + this.spit_id + '/',

                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response => {

                    this.spit.is_collection = false
                    this.spit.collecton_count -= 1;
                })
                .catch(error => {

                })

        },
        // 评论点赞
        comment_like:function (comment) {
            if(!this.token){
                location.href = '/person-loginsign.html?next=/spit-detail.html?id='+this.spit_id
                return
            }


            axios.post(this.host + '/spit/commentlike/',
                {
                    comment: comment.id
                },
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response =>{
                    // 改变该条评论里面的数据
                    comment.is_like =true;
                    comment.like_count +=1;
            })
            .catch(error => {

            })

        },
        // 评论取消点赞
        remove_comment_like:function (comment) {
            // 如果用户没有登录
            if(!this.token){
                location.href = '/person-loginsign.html?next=/spit-detail.html?id='+this.spit_id
                return
            }
            axios.delete(host + '/spit/commentlikes/'+comment.id+'/',
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response =>{

                 comment.is_like =false;
                 comment.like_count -=1;
            })
            .catch(error => {

            })
        },
        //发布评论
        create_comment:function () {
            if(!this.token){
                location.href = '/person-loginsign.html?next=/spit-detail.html?id='+this.spit_id
                return
            }
            if(this.new_comment_content == ''){
                alert('评论不能为空')
                return
            }

            axios.post(this.host + '/spit/'+this.spit.id+'/comments/',
                {
                    content: this.new_comment_content,
                    spit_id:this.spit_id
                },
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response =>{
                    // 改变该条评论里面的数据
                    this.new_comment_content = ''
                    this.get_comment()
                    this.spit.comment_count +=1
            })
            .catch(error => {

            })

        },
        //改变　　发布评论框　的显示状态
        change_comment_post:function () {
            this.show_comment_post = !this.show_comment_post
        },

    },
})

