var allTag = new Vue({
    el: "#allTag",
    data: {
        // 页面中需要使用到的数据，键值对
        tags:[

        ]
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        axios.get("http://127.0.0.1:8000/tags_all/")
            .then(response => {
                // console.log(response)
            this.tags = response.data
            })
            .catch(error => {
            })
    },
    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})
