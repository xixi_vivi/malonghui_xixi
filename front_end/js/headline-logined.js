


// 分类
var vm = new Vue({
    el:"#headline",
    data:{
        // 页面中需要使用到的数据，键值对
        names: null,
        news:[],
        isActive: true,
        hasError: false,
        isFans:true,
        cid: 0,
        activeClass: get_query_string("category", "热门")


    },

    mounted:function () {
        // 一加载就需要做的，直接是代码
        this.cid = get_query_string("cid", 0);
        axios.get(host + "/index/category/")
        .then(response=>{
            this.names = response.data;
            // console.log(response.data)
        })
        .catch(error =>{

        });

        axios.get(host + "/index/?cid=" + this.cid)
        .then(response=>{
            this.news = response.data;
            // console.log(response.data)

        })

    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // acquire:function (name) {
        //     this.activeClass = name.name
        // },

    //     getItme:function(index) {
    //       this.activeClass = index;  // 把当前点击元素的index，赋值给activeClass
    //     },
    }
});




// axios.get(host+"/index/category/")
//         .then(response=>{
//             console.log(response.data)
//         })
//         .catch(error =>{});
var vm = new Vue({
    el: '#app',
    // 声明vues使用的模板变量语法
    delimiters: ['[[', ']]'],
    data: {
        host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        // f1_tab: 1, // 1F 标签页控制
        // f2_tab: 1, // 2F 标签页控制
        // f3_tab: 1, // 3F 标签页控制
    },
    methods: {
        // 退出
        logout: function(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/index.html';
        }
    }
});