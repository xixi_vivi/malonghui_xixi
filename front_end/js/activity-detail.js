var status = ''

Vue.filter('status', function (value) {
    if(value == '1'){
        var status = "立即报名"
        return status
    }else if(value == '2'){
        var status = "活动进行中"
        return status
    }else{
        var status = "活动结束"
        return status
    }
})


var aaa = new Vue({
    el:"#detail",
    data:{
        // 页面中需要使用到的数据，键值对
        activity: null,
        deadline: 0,
        now: 0,
        remain_time: 0,
        activity_status: '',
        user_id: sessionStorage.user_id || localStorage.user_id,
        success_signup: '报名成功',
        faile_signup: '立即报名',
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码

        axios.get('http://127.0.0.1:8000' + '/activity/' + this.get_query_string("id", 1) + '/', {
                    responseType: 'json'
            })
            .then(response => {
                this.activity = response.data;
                this.activity_status = response.data.status;
                if(this.activity_status == '1'){
                   this.activity_status = '立即报名'
                }else if(this.activity_status == '2'){
                    this.activity_status = "活动进行中"
                }else{
                    this.activity_status = "活动结束"
                };
                if(this.activity_status == '立即报名' && this.user_id != null){
                    axios.get('http://127.0.0.1:8000' + '/activity/' + this.get_query_string("id", 1) + '/' + this.user_id + '/' ,{
                    responseType: 'json'
            })
                .then(response => {
                    if(response.data.signup_status == false){
                        this.activity_status = this.faile_signup
                    }else{
                        this.activity_status = this.success_signup

                    }

                // this.activity_status = response.data.signup_status;
                })
                .catch(error => {
                if (error.response.status == 400) {
                    this.activity_status = this.faile_signup
                } else {
                    console.log(error.response.data);
                }
            })
                }
                this.deadline = new Date(response.data.apply_time).getTime() / 1000;
                this.now = new Date().getTime() / 1000;
                this.remain_time = this.deadline - this.now;
                var t1 = setInterval( () =>{ this.now += 1; this.remain_time = this.deadline - this.now;if(this.remain_time <= 0){
                   clearInterval(t1); this.remain_time = '';
                }}, 1000)

            })
            .catch(error => {
                if (error.response.status == 400) {
                } else {
                    console.log(error.response.data);
                }
            })

    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        // 获取url路径参数
        show: function(){
            if(this.user_id == null){
                alert("请先登录")
            }
            if(this.activity_status == this.faile_signup){
                 axios.post('http://127.0.0.1:8000' + '/activity/' + this.get_query_string("id", 1) + '/' + this.user_id + '/create/' ,{
                     avtivity_id: this.get_query_string("id", 1),
                     user_id: this.user_id,
                     signup_status: 1
                     },
                     {
            responseType: 'json'
            })
                .then(response => {
                    this.activity_status = this.success_signup

                // this.activity_status = response.data.signup_status;
                })
                .catch(error => {
                if (error.response.status == 400) {
                    this.activity_status = this.faile_signup
                } else {
                    console.log(error.response.data);
                }
            })
            }
            // else {
            //        alert("已经报名或者,请及时参加活动")
            //     }
        },
        get_query_string: function(name){
            var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)', 'i');
            var r = window.location.search.substr(1).match(reg);
            if (r != null) {
                return decodeURI(r[2]);
            }
            return null;
        }
    },
    filters: {
        second2dhs: function (value) {
            if(value == ''){return '';
            }
            let day = Math.floor(value / (60 * 60 * 24));
            let hour = Math.floor((value - 24 * 60 * 60 * day) / (60 * 60));
            let minute = Math.floor((value - 24 * 60 * 60 * day - 60 * 60 * hour) / 60);
            let second = Math.floor(value % 60);
            return day + "天" + hour + "小时" + minute + "分" + second + "秒"

            value = value.toString()
            return value.charAt(0).toUpperCase() + value.slice(1)
        }
    }
})