/**
 * Created by python on 19-1-15.
 */
/**
 * Created by python on 18-12-10.
 */
var vm = new Vue({
    el: '#apps',
    data:{
        host:host,
        token: sessionStorage.token || localStorage.token,
        content: "",
        headers:{},

    },
    mounted : function () {
        //  向后端发送请求，首先判断是否存在token
        CKEDITOR.replace('content')
        if(this.token){
            this.headers = {
                'Authorization': 'JWT ' + this.token
            }
        }

    },
    methods : {
        submit_spit:function () {
            // alert(1)
            if(!this.token){
                location.href = '/person-loginsign.html?next=/spit-submit.html'
                return
            }
            if(CKEDITOR.instances.content.getData()==''){
                alert('内容不能为空')
                return
            }

            axios.post(this.host + '/spit/',
                {
                    content:CKEDITOR.instances.content.getData()
                },
                {
                    headers: this.headers,
                    responseType: 'json'
                })
                .then(response =>{
                    //页面跳转
                    location.href = '/spit-index.html'
            })
            .catch(error => {

            })
        }
    }
})
