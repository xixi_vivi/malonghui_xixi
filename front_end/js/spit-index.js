/**
 * Created by python on 19-1-11.
 */
// axios.get("http://127.0.0.1:8000/spit/")
// .then(response=>{
//     // console.log(response)
// })
// .catch(error =>{})


var app = new Vue({
    el:"#left-list",
    data:{
        // 页面中需要使用到的数据 键值对
        spits:[],
        host,
        headers:{},
        token:sessionStorage.token || localStorage.token,
    },
    computed:{
        // 需要通过计算得到的数据 键值对 ,键是名称,值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的   直接是代码
        if(this.token){
            this.headers = {
                'Authorization': 'JWT ' + this.token
                // 'Authorization': '' + this.token
                        }
        }
        axios.get(host + "/spit/",
            {
                headers:this.headers,
                responseType:'json'
            })
        .then(response=>{
            // console.log(response)
            this.spits = response.data
        })
        .catch(error =>{})
            },
        methods:{
        //需要用到的函数  键值对 ,键是名称,值是匿名函
    }
})