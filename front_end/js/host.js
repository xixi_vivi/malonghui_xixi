var host = 'http://127.0.0.1:8000';
var host_q = 'http://127.0.0.1:8000';


function get_query_string(name,def){
    var reg = new RegExp('(^|&)' + name + '=([^&]*)(&|$)','i');
    var r = window.location.search.substr(1).match(reg);
    if (r != null){
        return decodeURI(r[2]);
    }
    return def;
};

// 1. str2date 字符串 转 日期
// 2. date2ymd 获取日期的 年月日
// 3. date2hms 获取日期的 时分秒
// 4. date2week 获取日期的 周几

Vue.filter('str2date', function (value) {
    return new Date(value)

});
Vue.filter('date2ymd', function (value) {
    return value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate()

});
Vue.filter('date2hms', function (value) {
    return value.getHours() + ":" + value.getMinutes() + ":" + value.getSeconds()

});

const WEEKS = ["周日", "周一", "周二", "周三", "周四", "周五", "周六"];
Vue.filter('date2week', function (value) {
    return WEEKS[value.getDay()]
});
