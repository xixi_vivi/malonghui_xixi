/**
 * Created by python on 19-1-13.
 */
/**
 * Created by python on 19-1-12.
 */
// var host = 'http://127.0.0.1:8000';

// Vue.filters('sex', function(value) {
//             if (value == 0) {return '男'}
//             else {return '女'}
//           });

var vm = new Vue({
    el: '#myfule',
    data: {
        host:'http://127.0.0.1:8000',
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        username: '',
        mobile: '',
        email: '',
        birthday:'',
        user_url: '',
        now_city:'',
        school:'',
        gender:'',
    },
    mounted: function(){
        // 判断用户的登录状态
        if (this.user_id && this.token) {
            axios.get(this.host + '/user/detail/', {
                    // 向后端传递JWT token的方法
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json',
                })
                .then(response => {
                    // 加载用户数据
                    // this.user_id = response.data.id;
                    this.username = response.data.username;
                    this.mobile = response.data.mobile;
                    this.email = response.data.email;
                    this.user_url = response.data.user_url;
                    this.now_city = response.data.now_city;
                    this.school = response.data.school;
                    // this.company =response.data.company;
                    this.birthday =response.data.birthday;
                    this.gender =response.data.gender;
                    this.address =response.data.address;

                    if (this.gender == 0){
                        this.gender = '男'
                    }else{
                        this.gender ='女'
                    }
                })
                .catch(error => {
                    if (error.response.status==401 || error.response.status==403) {
                        location.href = '/person-loginsign.html';
                    }
                });
        } else {
            location.href = '/person-loginsign.html';
        }
    },
    methods: {
        // 退出
        logout: function(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/headline-logined.html';
        },
        // 保存email
        save_email: function(){

        }
    }

});




 // Vue.filter('date2ymd', function (value) {
//     return value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate()
//
// });