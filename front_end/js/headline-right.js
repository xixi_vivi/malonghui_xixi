/**
 * Created by python on 19-1-11.
 */
// var host = 'http://127.0.0.1:8000'
var vm = new Vue({
    el: '#app',
    delimiters: ['[[', ']]'],
    data: {
        host,
        username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        token: sessionStorage.token || localStorage.token,
        // f1_tab: 1, // 1F 标签页控制
        // f2_tab: 1, // 2F 标签页控制
        // f3_tab: 1, // 3F 标签页控制
    },
    // mounted: function(){
        // this.get_cart();
    // },
    methods: {
        // 退出
        logout: function(){
            sessionStorage.clear();
            localStorage.clear();
            location.href = '/headline-logined.html';
        },
    }
});