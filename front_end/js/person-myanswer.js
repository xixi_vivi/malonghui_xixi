Vue.filter('str2date', function (value) {
  return new Date(value)
})


Vue.filter('date2md', function (value) {
    return (value.getMonth() + 1) + "月" + value.getDate() + '日'
})

// Vue.filter('date2hms', function (value) {
//     return value.getHours() + ":" + (value.getMinutes() + 1) + ":" + value.getSeconds()
//
// })

Vue.filter('useful', function (value) {
    if(value != false){
        var status = "有用";
        return status
    }else{
        var status = "无用";
        return status
    }
})

// var WEEKS = ["周日","周一","周二","周三","周四","周五","周六"]
//
// Vue.filter('date2week', function (value) {
//     var n = value.getDay()
//     return WEEKS[n]
// })


var aaa = new Vue({
    el:".answers",
    data:{
        // 页面中需要使用到的数据，键值对
        answers: [],
        user_name: localStorage.username || sessionStorage.username,
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get('http://127.0.0.1:8000' + '/answers/' + this.user_name + '/answer/', {
                    responseType: 'json'
            })
            .then(response => {
                this.answers = response.data
            })
            .catch(error => {
                if (error.response.status == 400) {
                } else {
                    console.log(error.response.data);
                }
            })

    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})