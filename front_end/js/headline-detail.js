var aaa = new Vue({
    el: "#apps",
    data: {
        // 页面中需要使用到的数据，键值对
        headline: {},
        comments: {},
        submitreply: "",
        com_author_id: "",
        parent_id:null,
        com_author:[],
    },
    computed: {
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数

    },
    mounted: function () {
        // 一加载就需要做的，直接是代码
        let news_comm = this.headline.id;
        axios.get(host + "/news/" + get_query_string("id", 1) + "/")
            .then(response => {
                this.headline = response.data;
                // console.log(response.data)
            })
            .catch(error => {
            });
        axios.get(host + "/news/comment/" + get_query_string("id") + "/")
            .then(response => {
                this.comments = response.data;

                console.log(response.data)
            });

    },

    methods: {
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
        toReply: function (event) {
            console.log(event.target)
            $(event.target).parents(".text").siblings(".edit-box").toggle(500);
        },


        on_submit: function (comment) {
            // console.log(comment)
            console.log(comment.com_author.id)
            // this.com_author_id= comment.com_author.id;
            // this.parent_id = comment.parent_id;

            axios.post(host + '/news/comment/'+ get_query_string("id") + "/", {
                content: this.submitreply,
                com_author_id: comment.id,
                // parent_id:comment.parent_id,
            }, {
                responseType: 'json'
            })
                .then(response => {
                    alert("回复成功")
                })
                .catch(error => {
                    console.log('回复失败');
                })


        }},
    }
)


// var aaa = new Vue({
//     el: "#app",
//     data: {
//
//         // 页面中需要使用到的数据，键值对
//         headline: {author_id: {}},
//         comments: [{user: {}, parent: {}}],
//         authornews: [],
//         problems: [],
//         content: [],
//         token: sessionStorage.token || localStorage.token,
//         show:false,
//     },
//     computed: {
//         // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
//     },
//     mounted: function () {
//         // 一加载就需要做的，直接是代码
//         // let id = get_query_string("id");
//         let id = this.$route.params.id;
//         axios.get(host + '/news/' + id + '/')
//             .then(response => {
//                 this.headline = response.data
//             })
//             .catch(error => {
//
//             });
//
//         axios.get(host + '/comments/' + id + '/')
//             .then(response => {
//                 this.comments = response.data
//                 // console.log(this.comments)
//             })
//             .catch(error => {
//                 console.log(error)
//             });
//         axios.get(host + '/news/' + id + '/')
//             .then(response => {
//                 this.authornews = response.data
//             })
//             .catch(error => {
//                 console.log(error)
//             });
//         //    获取热门问题
//         axios.get(host + '/answers/')
//             .then(response => {
//                 this.problems = response.data
//                 // console.log(this.comments)
//             })
//             .catch(error => {
//                 console.log(error)
//             });
//         // spits
//         axios.get(host + '/spit/')
//             .then(response => {
//                 this.spits = response.data
//                 // console.log(this.comments)
//             })
//             .catch(error => {
//                 console.log(error)
//             });
//     },
//     methods: {
//          reply: function () {
//                 if(this.show == false){
//                     this.show = true
//                 }
//                 else {
//                     this.show = false
//                 }
//             },
//         // 需要用到的函数，键值对 ，键是名称，值是匿名函数
//         addcomment: function (parent_id) {
//             let news_id = get_query_string('id')
//             let news_content = {"content": this.content, "parent": parent_id}
//             axios.post(host + '/news/comment/' + news_id, news_content, {
//                 headers: {
//                     "Authorization": "JWT " + this.token
//                 },
//                 responseType: "json",
//             })
//                 .then(response => {
//                     // 请求成功则添加到评论列表里面
//
//                     this.contents = response.data;
//                     window.location.reload()
//                 })
//                 .catch(error => {
//                     console.log(error.response.data)
//                 });
//
//         }
//
//
//     }
// })
// //