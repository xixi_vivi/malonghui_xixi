Vue.filter('str2date', function (value) {
  return new Date(value)
})


Vue.filter('date2ymd', function (value) {
    return value.getFullYear() + "-" + (value.getMonth() + 1) + "-" + value.getDate()
})

Vue.filter('date2hms', function (value) {
    return value.getHours() + ":" + (value.getMinutes() + 1) + ":" + value.getSeconds()

})

var WEEKS = ["周日","周一","周二","周三","周四","周五","周六"]

Vue.filter('date2week', function (value) {
    var n = value.getDay()
    return WEEKS[n]
})

var status = ''

Vue.filter('status', function (value) {
    if(value == '1'){
        var status = "立即报名"
        return status
    }else if(value == '2'){
        var status = "活动进行中"
        return status
    }else{
        var status = "活动结束"
        return status
    }
})


var aaa = new Vue({
    el:"#detail",
    data:{
        // 页面中需要使用到的数据，键值对
        activities: [],
    },
    computed:{
        // 需要通过计算得到的数据，键值对 ，键是名称，值是匿名函数
    },
    mounted:function () {
        // 一加载就需要做的，直接是代码
        axios.get('http://127.0.0.1:8000' + '/activity/', {
                    responseType: 'json'
            })
            .then(response => {
                this.activities = response.data
            })
            .catch(error => {
                if (error.response.status == 400) {
                } else {
                    console.log(error.response.data);
                }
            })

    },
    methods:{
        // 需要用到的函数，键值对 ，键是名称，值是匿名函数
    }
})