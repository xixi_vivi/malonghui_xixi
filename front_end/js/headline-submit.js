/**
 * Created by python on 19-1-16.
 */

var vm = new Vue({
    el:"#submit_app",
    data:{
                // 用户的变量　判断用户是否登录
        //　表示从本地获取还是从session中获取　
        // 当用户登录时，服务器会把用户的名字，ｉｄ,图片链接返回给浏览器
        // 如果浏览器选择了记住密码则　吧这些信息存储到本地缓存中localStorage  反之存储到session中
        q_username: sessionStorage.username || localStorage.username,
        user_id: sessionStorage.user_id || localStorage.user_id,
        // user_img_url:sessionStorage.img_url || localStorage.img_url,
        token: sessionStorage.token || localStorage.token,
        // 存储的是访问的地址127.0.0.1:8000
        host:host,
        host_q:host_q,
    //    保存分类列表
        category_list:[],
        // 存储头条的标题
        headline_title:"",
    //    存储头条分类的id
        headline_cate:[],
    //    存储头条的摘要
        headline_desc:"",

        category_id:"3",
        //存储选中分类的id
        selectId:'',

    },
    mounted:function () {

          //
    },
    methods:{
    //    提交的js
        submit_headline:function () {
        //    获取表单的标题
        //    获取表单分类的id
        //    获取标签也就是摘要
        //    获取文本域内容
            var content_edict = CKEDITOR.instances.editor1.getData();
            // alert(content_edict)
            axios.post(this.host+'/news/publish/',{
                author_id:this.user_id,
                title:this.headline_title,
                category:this.category_id,
                digest:this.headline_desc,
                content:content_edict,
            },{
                    headers: {
                        'Authorization': 'JWT ' + this.token
                    },
                    responseType: 'json'
                }
            )
            .then(response =>{
                window.location.href='/headline-logined.html?cid=3'
            })
            .catch(error => {
                console.log(error)
            })
        }
    }
})
